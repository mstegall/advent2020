package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func loadData() (data []int) {
	b, err := ioutil.ReadFile("./cmd/day1/data")
	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(b), "\n")

	for _, line := range lines {
		i, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}
		data = append(data, i)
	}

	return
}

func findMatch(data []int, target int) (int, int, bool) {
	seen := map[int]bool{}

	for _, i := range data {
		compliment := target - i

		if _, ok := seen[compliment]; ok {
			return i, compliment, true
		}

		seen[i] = true
	}

	return 0, 0, false
}

func part1() {
	data := loadData()

	a, b, _ := findMatch(data, 2020)

	fmt.Println(a * b)
}

func part2() {
	data := loadData()

	for _, i := range data {
		a, b, ok := findMatch(data, 2020-i)

		if ok {
			fmt.Println(a * b * i)
		}
	}
}
