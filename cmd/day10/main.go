package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func loadData() (data []int) {
	d, err := ioutil.ReadFile("./cmd/day10/data")
	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(d), "\n") {
		num, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}
		data = append(data, num)
	}

	return
}

func part1() {
	d := loadData()

	sort.Ints(d)

	ones, threes := 0, 0

	if d[0] == 1 {
		ones++
	}

	if d[0] == 3 {
		threes++
	}

	for i := range d {
		if i+1 == len(d) {
			threes++
			continue
		}

		diff := d[i+1] - d[i]

		if diff == 1 {
			ones++
		}

		if diff == 3 {
			threes++
		}
	}

	fmt.Println(ones * threes)
}

func part2() {
	d := loadData()
	d = append(d, 0)
	sort.Ints(d)
	counts := make([]int, len(d))
	counts[len(d)-1] = 1

	for i := len(d) - 2; i >= 0; i-- {
		var one, two, three int

		if i+1 < len(d) && d[i+1]-d[i] <= 3 {
			one = counts[i+1]
		}
		if i+2 < len(d) && d[i+2]-d[i] <= 3 {
			two = counts[i+2]
		}
		if i+3 < len(d) && d[i+3]-d[i] <= 3 {
			three = counts[i+3]
		}

		counts[i] = one + two + three
	}

	fmt.Println(counts[0])
}
