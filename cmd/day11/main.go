package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	part2()
}

func part1() {
	r := newRoom(4)
	r.load()
	r.genCoordsAdjacent()
	for r.step() {
	}

	fmt.Println(r.seatsFilled())
}

func part2() {
	r := newRoom(5)
	r.load()
	r.genCoordsSight()
	for r.step() {
	}

	fmt.Println(r.seatsFilled())
}

type room struct {
	data       [][]string
	coords     [][][][]int
	len        int
	width      int
	seenStates map[string]bool
	tolerance  int
}

func newRoom(tolerance int) *room {
	return &room{
		seenStates: make(map[string]bool),
		tolerance:  tolerance,
	}
}

func (r *room) seatsFilled() int {
	count := 0

	for i := 0; i < r.len; i++ {
		for j := 0; j < r.width; j++ {
			if r.data[i][j] == "#" {
				count++
			}
		}
	}

	return count
}

func (r *room) seatFilled(x, y int) bool {
	coords := r.coords[y][x]
	count := 0

	for _, i := range coords {
		a := i[0]
		b := i[1]

		if r.data[b][a] == "#" {
			count++
		}
	}

	return r.data[y][x] == "#" && count < r.tolerance || count == 0

}

func (r *room) step() bool {
	next := [][]string{}

	for i := 0; i < r.len; i++ {
		line := []string{}
		for j := 0; j < r.width; j++ {
			if r.data[i][j] == "." {
				line = append(line, ".")
			} else if r.seatFilled(j, i) {
				line = append(line, "#")
			} else {
				line = append(line, "L")
			}
		}
		next = append(next, line)
	}

	r.data = next
	return r.save()
}

func (r *room) firstSeatInSight(dx, dy, x, y int) ([]int, bool) {
	a, b := x+dx, y+dy

	for a >= 0 && a < r.width && b >= 0 && b < r.len {
		if r.data[b][a] == "L" {
			return []int{a, b}, true
		}

		a += dx
		b += dy
	}

	return nil, false
}

func (r *room) genCoordsSight() {
	for y := 0; y < r.len; y++ {
		line := [][][]int{}
		for x := 0; x < r.width; x++ {
			candidates := [][]int{{-1, -1}, {-1, 0}, {-1, +1}, {0, -1}, {0, +1}, {+1, -1}, {+1, 0}, {+1, +1}}
			coords := [][]int{}

			for _, i := range candidates {
				a := i[0]
				b := i[1]

				seat, ok := r.firstSeatInSight(a, b, x, y)

				if ok {
					coords = append(coords, seat)
				}
			}

			line = append(line, coords)
		}
		r.coords = append(r.coords, line)
	}
}

func (r *room) genCoordsAdjacent() {
	for y := 0; y < r.len; y++ {
		line := [][][]int{}
		for x := 0; x < r.width; x++ {
			candidates := [][]int{{x - 1, y - 1}, {x - 1, y}, {x - 1, y + 1}, {x, y - 1}, {x, y + 1}, {x + 1, y - 1}, {x + 1, y}, {x + 1, y + 1}}
			coords := [][]int{}

			for _, i := range candidates {
				a := i[0]
				b := i[1]

				if a < 0 || a >= r.width {
					continue
				}
				if b < 0 || b >= r.len {
					continue
				}

				coords = append(coords, i)
			}

			line = append(line, coords)
		}
		r.coords = append(r.coords, line)
	}
}

func (r *room) load() {
	d, err := ioutil.ReadFile("./cmd/day11/data")
	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(d), "\n") {
		r.data = append(r.data, strings.Split(line, ""))
	}

	r.len = len(r.data)
	r.width = len(r.data[0])
	r.save()
	return
}

func (r *room) save() (new bool) {
	lines := []string{}

	for _, line := range r.data {
		lines = append(lines, strings.Join(line, ""))
	}

	state := strings.Join(lines, "")

	if _, seen := r.seenStates[state]; seen {
		return false
	}

	r.seenStates[state] = true
	return true
}
