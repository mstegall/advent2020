package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func part1() {
	instructions := load()
	card := [][]int{{1, 0}, {0, -1}, {-1, 0}, {0, 1}}
	cardMap := map[rune][]int{
		'E': card[0],
		'S': card[1],
		'W': card[2],
		'N': card[3],
	}
	direction := 0
	x, y := 0, 0

	for _, i := range instructions {
		chars := []rune(i)
		action := chars[0]
		value, err := strconv.Atoi(string(chars[1:]))
		if err != nil {
			panic(err)
		}
		if action == 'R' {
			direction += value / 90
			direction %= 4
		} else if action == 'L' {
			direction += -value/90 + 4
			direction %= 4
		} else if action == 'F' {
			travelDir := card[direction]
			x += travelDir[0] * value
			y += travelDir[1] * value
		} else {
			travelDir := cardMap[action]
			x += travelDir[0] * value
			y += travelDir[1] * value
		}
	}

	fmt.Println(math.Abs(float64(x)) + math.Abs(float64(y)))
}

func part2() {
	instructions := load()
	card := [][]float64{{1, 0}, {0, -1}, {-1, 0}, {0, 1}}
	cardMap := map[rune][]float64{
		'E': card[0],
		'S': card[1],
		'W': card[2],
		'N': card[3],
	}
	wx, wy := 10., 1.
	x, y := 0., 0.

	for _, i := range instructions {
		chars := []rune(i)
		action := chars[0]
		value, err := strconv.Atoi(string(chars[1:]))
		if err != nil {
			panic(err)
		}
		if action == 'R' {
			wx, wy = rotate(wx, wy, float64(value))
		} else if action == 'L' {
			wx, wy = rotate(wx, wy, -float64(value))
		} else if action == 'F' {
			x += wx * float64(value)
			y += wy * float64(value)
		} else {
			travelDir := cardMap[action]
			wx += travelDir[0] * float64(value)
			wy += travelDir[1] * float64(value)
		}

	}

	fmt.Println(math.Abs(x) + math.Abs(y))
}

func rotate(x, y, dt float64) (float64, float64) {
	m := math.Sqrt(x*x + y*y)
	t := math.Atan(y / x)

	dtr := dt / 180. * math.Pi

	if x < 0 {
		t += math.Pi
	}

	return m * math.Cos(t-dtr), m * math.Sin(t-dtr)
}

func load() []string {
	d, err := ioutil.ReadFile("./cmd/day12/data")
	if err != nil {
		panic(err)
	}

	return strings.Split(string(d), "\n")
}
