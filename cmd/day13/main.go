package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func part1() {
	arrival, buses := load()

	delay := math.Ceil(float64(arrival)/float64(buses[0]))*float64(buses[0]) - float64(arrival)
	fastest := 0

	for i, bus := range buses {

		newDelay := math.Ceil(float64(arrival)/float64(bus))*float64(bus) - float64(arrival)
		fmt.Println(delay, newDelay)
		if newDelay < delay {
			delay = newDelay
			fastest = i
		}
	}

	fmt.Println(float64(buses[fastest]) * delay)
}

func part2() {
	buses := load2()
	t := 0
	factor := 1
	fmt.Println(buses)

	for i, bus := range buses {
		if bus == "x" {
			continue
		}
		busTime, err := strconv.Atoi(bus)
		if err != nil {
			panic(err)
		}
		fmt.Println((t + i) % busTime)
		for (t+i)%busTime != 0 {
			t += factor
		}

		factor *= busTime
	}

	fmt.Println(t)
}

func load() (int, []int) {
	d, err := ioutil.ReadFile("./cmd/day13/data")
	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(d), "\n")
	arrival, err := strconv.Atoi(lines[0])
	if err != nil {
		panic(err)
	}
	buses := []int{}
	for _, i := range strings.Split(lines[1], ",") {
		if i == "x" {
			continue
		}
		bus, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}

		buses = append(buses, bus)
	}

	return arrival, buses
}

func load2() []string {
	d, err := ioutil.ReadFile("./cmd/day13/data")
	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(d), "\n")

	return strings.Split(lines[1], ",")
}
