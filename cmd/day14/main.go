package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func part1() {
	instructions := load()
	memory := make(map[string]int)

	andMask := 0
	orMask := 0

	assignRegex := regexp.MustCompile(`mem\[(\d*)\] = (\d*)`)

	for _, inst := range instructions {
		if strings.HasPrefix(inst, "mask") {
			andMask, orMask = parseMask(strings.Split(inst, " = ")[1])
			continue
		}

		match := assignRegex.FindSubmatch([]byte(inst))
		address := string(match[1])
		value, err := strconv.Atoi(string(match[2]))
		if err != nil {
			panic(err)
		}

		memory[address] = (value & andMask) | orMask
	}

	sum := 0

	for _, v := range memory {
		sum += v
	}

	fmt.Println(sum)
}

func part2() {
	instructions := load()
	memory := make(map[int]int)

	assignRegex := regexp.MustCompile(`mem\[(\d*)\] = (\d*)`)

	mask := ""

	for _, inst := range instructions {
		if strings.HasPrefix(inst, "mask") {
			mask = strings.Split(inst, " = ")[1]
			continue
		}

		match := assignRegex.FindSubmatch([]byte(inst))
		address, err := strconv.Atoi(string(match[1]))
		if err != nil {
			panic(err)
		}
		value, err := strconv.Atoi(string(match[2]))
		if err != nil {
			panic(err)
		}

		for _, addr := range parseAddressMask(mask, address) {
			memory[addr] = value
		}

	}

	sum := 0

	for _, v := range memory {
		sum += v
	}

	fmt.Println(sum)
}

func load() []string {
	d, err := ioutil.ReadFile("./cmd/day14/data")
	if err != nil {
		panic(err)
	}

	return strings.Split(string(d), "\n")
}

func parseMask(mask string) (and int, or int) {
	for _, char := range []rune(mask) {
		and <<= 1
		or <<= 1

		switch char {
		case 'X':
			and++
		case '1':
			or++
		}
	}

	return
}

func parseAddressMask(mask string, address int) (addresses []int) {
	floating := []int{}
	and, or := 0, 0

	for i, char := range []rune(mask) {
		and <<= 1
		or <<= 1

		switch char {
		case '0':
			and++
		case '1':
			or++
		case 'X':
			floating = append(floating, len(mask)-i-1)
		}
	}

	bits := len(floating)

	for i := 0; i < int(math.Pow(2, float64(bits))); i++ {
		base := (address & and) | or

		for j := 0; j < bits; j++ {
			base |= ((i >> j) & 1) << floating[j]
		}

		addresses = append(addresses, base)
	}

	return
}
