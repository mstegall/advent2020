package main

import "fmt"

// var starter = []int{1, 3, 2}

var starter = []int{18, 11, 9, 0, 5, 1}

func main() {
	part1()
}

func part1() {
	// saidMap := map[int]int{}
	saidMap := make([]int, 30000000)
	okMap := make([]bool, 30000000)
	//said := []int{}
	last := 0

	for i := 0; i < 30000000; i++ {
		//	said = append(said, last)
		if i < len(starter) {
			if i != 0 {
				saidMap[last] = i - 1
				okMap[last] = true
			}
			last = starter[i]
			continue
		}

		ok := okMap[last]
		lastSaid := saidMap[last]
		saidMap[last] = i - 1
		okMap[last] = true
		if !ok {
			last = 0
		} else {
			last = i - lastSaid - 1
		}

	}
	//said = append(said, last)
	//fmt.Println(said)
	fmt.Println(last)
}
