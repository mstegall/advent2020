package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type ruleset = [][2][2]int

func main() {
	part2()
}

func part2() {
	rules, _, mine, tickets := loadData()

	validTickets := getValidTickets(tickets, rules)

	ruleOptions := [][]int{}
	for _, rule := range rules {
		options := []int{}
	fieldLoop:
		for i := range validTickets[0] {
			for _, t := range validTickets {
				if !(inRange(t[i], rule[0]) || inRange(t[i], rule[1])) {
					continue fieldLoop
				}
			}

			options = append(options, i)
		}
		ruleOptions = append(ruleOptions, options)
	}

	// fmt.Println(ruleOptions)

	finals := make([]int, len(rules))

outer:
	for {
		for i, v := range ruleOptions {
			if len(v) != 1 {
				continue
			}

			finals[i] = v[0]

			for j, opts := range ruleOptions {
				ruleOptions[j] = remove(v[0], opts)
			}

			continue outer
		}

		break
	}

	total := 1

	for _, i := range finals[:6] {
		total *= mine[i]
	}

	fmt.Println(total)
}

func part1() {
	rules, _, _, tickets := loadData()

	errorRate := 0

	for _, ticket := range tickets {
		for _, v := range ticket {

			if !isValid(v, rules) {

				errorRate += v
			}
		}
	}

	fmt.Println(errorRate)
}

func remove(i int, is []int) (r []int) {
	for _, v := range is {
		if i != v {
			r = append(r, v)
		}
	}

	return
}

func getValidTickets(tickets [][]int, rules ruleset) (valid [][]int) {
	for _, t := range tickets {
		if isValidTicket(t, rules) {
			valid = append(valid, t)
		}
	}

	return
}

func isValidTicket(t []int, r ruleset) bool {
	for _, v := range t {
		if !isValid(v, r) {
			return false
		}
	}

	return true
}

func isValid(i int, r ruleset) bool {
	for _, rule := range r {
		if inRange(i, rule[0]) || inRange(i, rule[1]) {
			return true
		}
	}
	return false
}

func inRange(i int, r [2]int) bool {
	return i >= r[0] && i <= r[1]
}

func loadData() (allowed [][2][2]int, ruleNames []string, mine []int, others [][]int) {
	d, err := ioutil.ReadFile("./cmd/day16/data")
	if err != nil {
		panic(err)
	}

	sections := strings.Split(string(d), "\n\n")

	for _, rule := range strings.Split(sections[0], "\n") {
		trimmed := strings.Split(rule, ": ")[1]
		ruleNames = append(ruleNames, strings.Split(rule, ": ")[0])
		ranges := strings.Split(trimmed, " or ")

		range1Strings := strings.Split(ranges[0], "-")
		range1Lower, err := strconv.Atoi(range1Strings[0])
		if err != nil {
			panic(err)
		}
		range1Upper, err := strconv.Atoi(range1Strings[1])
		if err != nil {
			panic(err)
		}

		range2Strings := strings.Split(ranges[1], "-")
		range2Lower, err := strconv.Atoi(range2Strings[0])
		if err != nil {
			panic(err)
		}
		range2Upper, err := strconv.Atoi(range2Strings[1])
		if err != nil {
			panic(err)
		}

		allowed = append(allowed, [2][2]int{{range1Lower, range1Upper}, {range2Lower, range2Upper}})
	}

	for _, num := range strings.Split(strings.Split(sections[1], "\n")[1], ",") {
		v, err := strconv.Atoi(num)
		if err != nil {
			panic(err)
		}
		mine = append(mine, v)
	}

	for _, line := range strings.Split(sections[2], "\n")[1:] {
		lineValues := []int{}

		for _, num := range strings.Split(line, ",") {
			v, err := strconv.Atoi(num)
			if err != nil {
				panic(err)
			}
			lineValues = append(lineValues, v)
		}

		others = append(others, lineValues)
	}

	return
}
