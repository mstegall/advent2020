package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	part2()
}

func part2() {
	old := newCube()
	new := newCube()

	d, err := ioutil.ReadFile("./cmd/day17/data")
	if err != nil {
		panic(err)
	}
	for i, line := range strings.Split(string(d), "\n") {
		for j, char := range []rune(line) {
			if char == '#' {
				old.set(j, i, 0, 0, true)
			}
		}
	}

	active := 0
	for i := 0; i < 6; i++ {
		active = 0

		for x := -old.dataBound - 1; x <= old.dataBound+1; x++ {
			for y := -old.dataBound - 1; y <= old.dataBound+1; y++ {
				for z := -old.dataBound - 1; z <= old.dataBound+1; z++ {
					for w := -old.dataBound - 1; w <= old.dataBound+1; w++ {
						count := old.neighborCount(x, y, z, w)
						if count == 3 || (count == 2 && old.get(x, y, z, w)) {
							new.set(x, y, z, w, true)
							active++
						} else {
							new.set(x, y, z, w, false)
						}
					}
				}
			}
		}

		new, old = old, new
	}

	fmt.Println(active)
}

type cube struct {
	data      [][][][]bool
	offset    int
	bound     int
	dataBound int
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func newCube() *cube {
	data := [][][][]bool{}

	for h := 0; h < 5; h++ {
		cube := [][][]bool{}
		for i := 0; i < 5; i++ {
			plane := [][]bool{}
			for j := 0; j < 5; j++ {
				plane = append(plane, make([]bool, 5))
			}
			cube = append(cube, plane)
		}
		data = append(data, cube)
	}

	return &cube{
		data:   data,
		offset: 2,
		bound:  2,
	}
}

func (c *cube) boundsCheck(x, y, z, w int) bool {
	return abs(x) > c.bound || abs(y) > c.bound || abs(z) > c.bound || abs(w) > c.bound
}

func max(x, y, z, w int) int {
	max := x
	if y > max {
		max = y
	}
	if z > max {
		max = z
	}
	if w > max {
		max = w
	}
	return max
}

var neighbors [][4]int

func init() {
	for x := -1; x < 2; x++ {
		for y := -1; y < 2; y++ {
			for z := -1; z < 2; z++ {
				for w := -1; w < 2; w++ {
					if x == 0 && y == 0 && z == 0 && w == 0 {
						continue
					}
					neighbors = append(neighbors, [4]int{x, y, z, w})
				}
			}
		}
	}
}

func (c *cube) neighborCount(x, y, z, w int) (count int) {
	for _, n := range neighbors {
		if c.get(x+n[0], y+n[1], z+n[2], w+n[3]) {
			count++
		}
	}

	return
}

func (c *cube) grow(x, y, z, w int) {
	max := max(abs(x), abs(y), abs(z), abs(w))

	newBound := max * 2
	dB := newBound - c.bound

	data := [][][][]bool{}

	size := newBound*2 + 1

	for h := 0; h < size; h++ {
		cube := [][][]bool{}
		for i := 0; i < size; i++ {
			plane := [][]bool{}
			for j := 0; j < size; j++ {
				plane = append(plane, make([]bool, size))
				if h >= dB && h < size-dB && j >= dB && i >= dB && j < size-dB && i < size-dB {
					copy(plane[j][dB:], c.data[h-dB][i-dB][j-dB])
				}
			}
			cube = append(cube, plane)
		}
		data = append(data, cube)
	}

	c.data = data
	c.bound = newBound
	c.offset = newBound
}

func (c *cube) get(x, y, z, w int) bool {
	if c.boundsCheck(x, y, z, w) {
		return false
	}

	return c.data[c.offset+z][c.offset+y][c.offset+x][c.offset+w]
}

func (c *cube) set(x, y, z, w int, b bool) {
	if c.boundsCheck(x, y, z, w) {
		c.grow(x, y, z, w)
	}

	max := max(x, y, z, w)
	if max > c.dataBound {
		c.dataBound = max
	}

	c.data[c.offset+z][c.offset+y][c.offset+x][c.offset+w] = b
}

func (c *cube) fmtLayer(i int) (out string) {
	for _, line := range c.data[c.offset][c.offset+i] {
		for _, v := range line {
			if v {
				out += "#"
			} else {
				out += "."
			}
		}
		out += "\n"
	}
	return
}
