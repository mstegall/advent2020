package maincube

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	part1()
}

func part1() {
	old := newCube()
	new := newCube()

	d, err := ioutil.ReadFile("./cmd/day17/data")
	if err != nil {
		panic(err)
	}
	for i, line := range strings.Split(string(d), "\n") {
		for j, char := range []rune(line) {
			if char == '#' {
				old.set(j, i, 0, true)
			}
		}
	}

	active := 0
	for i := 0; i < 6; i++ {
		active = 0

		for x := -old.dataBound - 1; x <= old.dataBound+1; x++ {
			for y := -old.dataBound - 1; y <= old.dataBound+1; y++ {
				for z := -old.dataBound - 1; z <= old.dataBound+1; z++ {
					count := old.neighborCount(x, y, z)
					if count == 3 || (count == 2 && old.get(x, y, z)) {
						new.set(x, y, z, true)
						active++
					} else {
						new.set(x, y, z, false)
					}
				}
			}
		}

		new, old = old, new
	}

	fmt.Println(active)
}

type cube struct {
	data      [][][]bool
	offset    int
	bound     int
	dataBound int
}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}

func newCube() *cube {
	data := [][][]bool{}

	for i := 0; i < 5; i++ {
		plane := [][]bool{}
		for j := 0; j < 5; j++ {
			plane = append(plane, make([]bool, 5))
		}
		data = append(data, plane)
	}

	return &cube{
		data:   data,
		offset: 2,
		bound:  2,
	}
}

func (c *cube) boundsCheck(x, y, z int) bool {
	return abs(x) > c.bound || abs(y) > c.bound || abs(z) > c.bound
}

func max(x, y, z int) int {
	max := x
	if y > max {
		max = y
	}
	if z > max {
		max = z
	}
	return max
}

var neighbors = [][3]int{{1, 1, 1}, {1, 0, 1}, {1, -1, 1}, {0, 1, 1}, {0, 0, 1}, {0, -1, 1}, {-1, 1, 1}, {-1, 0, 1}, {-1, -1, 1},
	{1, 1, 0}, {1, 0, 0}, {1, -1, 0}, {0, 1, 0}, {0, -1, 0}, {-1, 1, 0}, {-1, 0, 0}, {-1, -1, 0},
	{1, 1, -1}, {1, 0, -1}, {1, -1, -1}, {0, 1, -1}, {0, 0, -1}, {0, -1, -1}, {-1, 1, -1}, {-1, 0, -1}, {-1, -1, -1},
}

func (c *cube) neighborCount(x, y, z int) (count int) {
	for _, n := range neighbors {
		if c.get(x+n[0], y+n[1], z+n[2]) {
			count++
		}
	}

	return
}

func (c *cube) grow(x, y, z int) {
	max := max(abs(x), abs(y), abs(z))

	newBound := max * 2
	dB := newBound - c.bound

	data := [][][]bool{}

	size := newBound*2 + 1

	for i := 0; i < size; i++ {
		plane := [][]bool{}
		for j := 0; j < size; j++ {
			plane = append(plane, make([]bool, size))
			if j >= dB && i >= dB && j < size-dB && i < size-dB {
				copy(plane[j][dB:], c.data[i-dB][j-dB])
			}
		}
		data = append(data, plane)
	}

	c.data = data
	c.bound = newBound
	c.offset = newBound
}

func (c *cube) get(x, y, z int) bool {
	if c.boundsCheck(x, y, z) {
		return false
	}

	return c.data[c.offset+z][c.offset+y][c.offset+x]
}

func (c *cube) set(x, y, z int, b bool) {
	if c.boundsCheck(x, y, z) {
		c.grow(x, y, z)
	}

	max := max(x, y, z)
	if max > c.dataBound {
		c.dataBound = max
	}

	c.data[c.offset+z][c.offset+y][c.offset+x] = b
}

func (c *cube) fmtLayer(i int) (out string) {
	for _, line := range c.data[c.offset+i] {
		for _, v := range line {
			if v {
				out += "#"
			} else {
				out += "."
			}
		}
		out += "\n"
	}
	return
}
