package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	part2()
}

func part1() {
	sum := 0

	file, err := os.Open("./cmd/day18/data")
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		sum += evalExpression(scanner.Text())
	}

	fmt.Println(sum)
}

func part2() {
	sum := 0

	file, err := os.Open("./cmd/day18/data")
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		sum += evalExpression2(scanner.Text())
	}

	fmt.Println(sum)
}

func evalExpression(expression string) int {
	stack := []*struct {
		acc int
		op  rune
	}{{}}

	for _, char := range []rune(expression) {
		switch char {
		case ' ':
			continue
		case '*':
			stack[len(stack)-1].op = char
		case '+':
			stack[len(stack)-1].op = char
		case '(':
			stack = append(stack, &struct {
				acc int
				op  rune
			}{})
		case ')':
			popped := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			topFrame := stack[len(stack)-1]
			if topFrame.op == '*' {
				topFrame.acc *= popped.acc
				topFrame.op = ' '
				continue
			}

			if topFrame.op == '+' {
				topFrame.acc += popped.acc
				topFrame.op = ' '
				continue
			}
			topFrame.acc = popped.acc
		default:
			d, err := strconv.Atoi(string(char))
			if err != nil {
				panic(err)
			}

			topFrame := stack[len(stack)-1]

			if topFrame.op == '*' {
				topFrame.acc *= d
				topFrame.op = ' '
				continue
			}

			if topFrame.op == '+' {
				topFrame.acc += d
				topFrame.op = ' '
				continue
			}
			topFrame.acc = d
		}
	}

	return stack[0].acc
}

func evalExpression2(expression string) int {
	stack := []*struct {
		acc   int
		op    rune
		paren bool
	}{{}}

	chars := []rune{}
	for _, char := range []rune(expression) {
		if char != ' ' {
			chars = append(chars, char)
		}
	}

	for i, char := range chars {
		switch char {
		case '*':
			stack[len(stack)-1].op = char
			if i+2 < len(chars) && chars[i+2] == '+' {
				stack = append(stack, &struct {
					acc   int
					op    rune
					paren bool
				}{})
			}
		case '+':
			stack[len(stack)-1].op = char
		case '(':
			stack = append(stack, &struct {
				acc   int
				op    rune
				paren bool
			}{paren: true})
		case ')':
			// eval all pending multiplies until parens
			for len(stack) > 1 && !stack[len(stack)-1].paren {
				popped := stack[len(stack)-1]
				stack = stack[:len(stack)-1]
				topFrame := stack[len(stack)-1]

				topFrame.acc *= popped.acc

			}

			popped := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			topFrame := stack[len(stack)-1]

			if topFrame.op == '*' {
				if len(chars) == i+1 || chars[i+1] != '+' {
					topFrame.acc *= popped.acc
					topFrame.op = ' '
					continue
				}
				popped.paren = false
				stack = append(stack, popped)
				continue
			}

			if topFrame.op == '+' {
				topFrame.acc += popped.acc
				topFrame.op = ' '

				continue
			}
			topFrame.acc = popped.acc
		default:
			d, err := strconv.Atoi(string(char))
			if err != nil {
				panic(err)
			}

			topFrame := stack[len(stack)-1]

			if topFrame.op == '*' {
				topFrame.acc *= d
				topFrame.op = ' '

				continue
			}

			if topFrame.op == '+' {
				topFrame.acc += d
				topFrame.op = ' '
				// eval all pending multiplies until parens
				if i+1 == len(chars) || chars[i+1] == '*' {
					for len(stack) > 1 && !stack[len(stack)-1].paren {
						popped := stack[len(stack)-1]
						stack = stack[:len(stack)-1]
						topFrame := stack[len(stack)-1]

						topFrame.acc *= popped.acc
					}
				}

				continue
			}

			topFrame.acc = d
		}
	}

	for len(stack) > 1 && !stack[len(stack)-1].paren {
		popped := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		topFrame := stack[len(stack)-1]

		if topFrame.op == '*' {
			topFrame.acc *= popped.acc
			topFrame.op = ' '
			continue
		}

		if topFrame.op == '+' {
			topFrame.acc += popped.acc
			topFrame.op = ' '

			continue
		}
	}

	return stack[0].acc
}
