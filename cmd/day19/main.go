package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func part1() {
	rules, passwords := loadData()

	count := 0

	for _, p := range passwords {
		match, remainder := checkRule(rules, 0, []string{p})

		if match {
			for _, r := range remainder {
				if r == "" {
					count++
				}
			}
		}
	}

	fmt.Println(count)
}

func part2() {
	rules, passwords := loadData()

	count := 0

	rules[8] = [][]int{{42}, {42, 8}}
	rules[11] = [][]int{{42, 31}, {42, 11, 31}}

	for _, p := range passwords {
		match, remainder := checkRule(rules, 0, []string{p})

		if match {
			for _, r := range remainder {
				if r == "" {
					count++
				}
			}
		}
	}

	fmt.Println(count)
}

func checkRule(rules map[int]interface{}, ruleNo int, inputs []string) (bool, []string) {
	ruleI := rules[ruleNo]

	switch rule := ruleI.(type) {
	case [][]int:
		remainders := []string{}

		for _, i := range inputs {
		outerLoop:
			for _, r := range rule {
				remainder := []string{i}
				for _, c := range r {
					var ok bool
					ok, remainder = checkRule(rules, c, remainder)
					if !ok {
						continue outerLoop
					}

				}
				remainders = append(remainders, remainder...)
			}
		}

		return len(remainders) > 0, remainders
	case string:
		remainders := []string{}

		for _, input := range inputs {
			if strings.HasPrefix(input, rule) {
				remainders = append(remainders, strings.TrimPrefix(input, rule))
			}
		}

		return len(remainders) > 0, remainders
	default:
		panic("bad type")
	}
}

func loadData() (map[int]interface{}, []string) {
	d, err := ioutil.ReadFile("./data")
	if err != nil {
		panic(err)
	}

	sections := strings.Split(string(d), "\n\n")

	rules := map[int]interface{}{}
	passwords := strings.Split(sections[1], "\n")

	for _, line := range strings.Split(sections[0], "\n") {
		parts := strings.Split(line, ": ")

		ruleNo, err := strconv.Atoi(parts[0])
		if err != nil {
			panic(err)
		}

		if strings.Contains(parts[1], `"`) {
			rules[ruleNo] = strings.Trim(parts[1], `"`)
			continue
		}

		setsRaw := strings.Split(parts[1], " | ")

		sets := [][]int{}
		for _, raw := range setsRaw {
			rules := []int{}
			for _, rule := range strings.Split(raw, " ") {
				num, err := strconv.Atoi(rule)
				if err != nil {
					panic(err)
				}
				rules = append(rules, num)
			}
			sets = append(sets, rules)
		}

		rules[ruleNo] = sets
	}

	return rules, passwords
}
