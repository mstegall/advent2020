package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func isValid1(line string) bool {
	i := strings.Split(line, ":")
	j := strings.Split(i[0], " ")
	k := strings.Split(j[0], "-")

	pass := i[1]
	char := j[1]
	lower, err := strconv.Atoi(k[0])
	if err != nil {
		panic(err)
	}
	upper, err := strconv.Atoi(k[1])
	if err != nil {
		panic(err)
	}

	count := 0

	for _, c := range strings.Split(pass, "") {
		if c == char {
			count++

			if count > upper {
				return false
			}
		}
	}

	return count >= lower
}

func isValid2(line string) bool {
	i := strings.Split(line, ":")
	j := strings.Split(i[0], " ")
	k := strings.Split(j[0], "-")

	pass := strings.Trim(i[1], " ")
	char := j[1]
	lower, err := strconv.Atoi(k[0])
	if err != nil {
		panic(err)
	}
	upper, err := strconv.Atoi(k[1])
	if err != nil {
		panic(err)
	}

	chars := strings.Split(pass, "")
	// fmt.Println(line, (chars[lower-1]), (chars[upper-1]), (chars[lower-1] == char) != (chars[upper-1] == char))
	return (chars[lower-1] == char) != (chars[upper-1] == char)
}

func part1() {
	file, err := os.Open("./cmd/day2/data")
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	count := 0

	for scanner.Scan() {
		if isValid1(scanner.Text()) {
			count++
		}
	}

	fmt.Println(count)
}

func part2() {
	file, err := os.Open("./cmd/day2/data")
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	count := 0

	for scanner.Scan() {
		if isValid2(scanner.Text()) {
			count++
		}
	}

	fmt.Println(count)
}
