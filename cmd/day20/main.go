package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func main() {
	part2()
}

type edge struct {
	data string
	tile int
}

type tile struct {
	edges struct {
		n edge
		e edge
		s edge
		w edge
	}
	internal    [][]rune
	connections int
}

func (t *tile) flipHorizontal() {
	flipRunesH(t.internal)
	t.edges.n.data, t.edges.s.data = reverseString(t.edges.n.data), reverseString(t.edges.s.data)
	t.edges.e, t.edges.w = t.edges.w, t.edges.e
}

func (t *tile) flipVertical() {
	flipRunesV(t.internal)
	t.edges.n, t.edges.s = t.edges.s, t.edges.n
	t.edges.e.data, t.edges.w.data = reverseString(t.edges.e.data), reverseString(t.edges.w.data)
}

func (t *tile) rotate() {
	t.edges.n, t.edges.e, t.edges.s, t.edges.w = t.edges.w, t.edges.n, t.edges.e, t.edges.s
	t.edges.n.data, t.edges.s.data = reverseString(t.edges.n.data), reverseString(t.edges.s.data)

	rotate(t.internal)
}

func transposeRunes(r [][]rune) {
	sideLen := len(r[0])
	for i := 0; i < sideLen; i++ {
		for j := i; j < sideLen; j++ {
			r[i][j], r[j][i] = r[j][i], r[i][j]
		}
	}
}

func flipRunesH(r [][]rune) {
	for i, line := range r {
		r[i] = reverseRunes(line)
	}
}

func flipRunesV(r [][]rune) {
	transposeRunes(r)
	flipRunesH(r)
	transposeRunes(r)
}

func rotate(r [][]rune) {
	transposeRunes(r)
	flipRunesH(r)
}

func (t *tile) permute(test func(t *tile) bool) {
	for i := 0; i < 8; i++ {
		if test(t) {
			return
		}

		// if i == 3 || i == 11 {
		// 	t.flipHorizontal()
		// }
		if i == 3 {
			t.flipVertical()
		}

		t.rotate()
	}

	panic("FAILED")
}

func printRunes(r [][]rune) {
	for _, row := range r {
		fmt.Println(string(row))
	}
}

func part2() {
	_, tileMap := loadData()

	stiched := [][]*tile{}
	sideLen := int(math.Sqrt(float64(len(tileMap))))

	for y := 0; y < sideLen; y++ {
		line := []*tile{}
		for x := 0; x < sideLen; x++ {
			if x == 0 {
				if y == 0 {
					for _, t := range tileMap {
						if t.connections == 2 {
							line = append(line, t)

							t.permute(func(t *tile) bool {
								return t.edges.n.tile == 0 && t.edges.w.tile == 0
							})
							break
						}
					}
					continue
				}

				match := stiched[y-1][0]

				t := tileMap[match.edges.s.tile]

				t.permute(func(t *tile) bool {
					return t.edges.n.data == match.edges.s.data
				})

				line = append(line, t)
			} else {
				match := line[x-1]

				t := tileMap[match.edges.e.tile]

				t.permute(func(t *tile) bool {
					return t.edges.w.data == match.edges.e.data
				})

				line = append(line, t)
			}

		}
		stiched = append(stiched, line)
	}

	tileHeight := len(stiched[0][0].internal)

	fullImage := [][]rune{}

	for _, row := range stiched {
		rowRunes := make([][]rune, tileHeight)
		for i := range rowRunes {
			rowRunes[i] = []rune{}
		}

		for _, tile := range row {
			for i, d := range tile.internal {
				rowRunes[i] = append(rowRunes[i], d...)
			}
		}

		fullImage = append(fullImage, rowRunes...)
	}

	monsterCoords := map[int]map[int]bool{}

	for i := 0; i < 8; i++ {
		count := 0
		for i, row := range fullImage {
			for j := range row {
				monsterFound, coords := checkMonster(fullImage, i, j)

				if monsterFound {
					count++
					for _, c := range coords {
						if monsterCoords[c[0]] == nil {
							monsterCoords[c[0]] = map[int]bool{}
						}

						monsterCoords[c[0]][c[1]] = true
					}
				}
			}
		}

		if count != 0 {
			break
		}

		if i == 4 {
			flipRunesV(fullImage)
		}

		rotate(fullImage)
	}

	count := 0
	for y, row := range fullImage {
		for x, char := range row {

			if char == '#' && (monsterCoords[y] == nil || !monsterCoords[y][x]) {
				count++
			}

			if monsterCoords[y] != nil && monsterCoords[y][x] {
				fmt.Printf("\033[1m\033[31mO\033[0m")
			} else {
				fmt.Printf(string(char))
			}

			if x == len(row)-1 {
				fmt.Printf("\n")
			}
		}
	}

	fmt.Println(count)
}

var monster = [][]rune{
	[]rune(`                  # `),
	[]rune(`#    ##    ##    ###`),
	[]rune(` #  #  #  #  #  #   `),
}

func checkMonster(data [][]rune, x, y int) (present bool, coords [][2]int) {
	if len(data) < y+len(monster) || len(data[0]) < x+len(monster[0]) {
		return
	}

	for i, row := range monster {
		for j, char := range row {
			if char == '#' {
				if data[y+i][x+j] != '#' {
					return
				}
				coords = append(coords, [2]int{y + i, x + j})
			}
		}
	}

	return true, coords
}

func part1() {
	_, tileMap := loadData()

	result := 1
	for i, t := range tileMap {
		if t.connections == 2 {
			result *= i
		}
	}

	fmt.Println(result)
}

func loadData() (edgeMap map[string][]int, tileMap map[int]*tile) {
	d, err := ioutil.ReadFile("./cmd/day20/data")
	if err != nil {
		panic(err)
	}

	edgeMap = map[string][]int{}
	tileMap = map[int]*tile{}

	for _, tileData := range strings.Split(string(d), "\n\n") {
		lines := strings.Split(tileData, "\n")

		idS := strings.TrimPrefix(strings.TrimSuffix(lines[0], ":"), "Tile ")
		id, err := strconv.Atoi(idS)
		if err != nil {
			panic(err)
		}

		t := &tile{}

		edges := []string{}
		left, right, leftR, rightR := make([]rune, len(lines[1:])), make([]rune, len(lines[1:])), make([]rune, len(lines[1:])), make([]rune, len(lines[1:]))

		for i, line := range lines[1:] {
			if i == 0 {
				edges = append(edges, line, reverseString(line))
				t.edges.n.data = line
			}
			if i == len(lines[1:])-1 {
				edges = append(edges, line, reverseString(line))
				t.edges.s.data = line
			}
			chars := []rune(line)
			left[i] = chars[0]
			right[i] = chars[len(chars)-1]
			leftR[len(leftR)-1-i] = chars[0]
			rightR[len(leftR)-1-i] = chars[len(chars)-1]

			if i != 0 && i != len(lines[1:])-1 {

				t.internal = append(t.internal, []rune(line)[1:len(line)-1])
			}
		}

		t.edges.w.data = string(left)
		t.edges.e.data = string(right)

		edges = append(edges, string(left), string(right), string(leftR), string(rightR))

		for _, edge := range edges {
			mapEntry, ok := edgeMap[edge]
			if ok {
				edgeMap[edge] = append(mapEntry, id)
			} else {
				edgeMap[edge] = []int{id}
			}
		}
		tileMap[id] = t
	}

	for id, tile := range tileMap {
		if tileList, _ := edgeMap[tile.edges.e.data]; len(tileList) > 1 {
			tile.connections++
			if tileList[0] != id {
				tile.edges.e.tile = tileList[0]
			} else {
				tile.edges.e.tile = tileList[1]
			}
		}
		if tileList, _ := edgeMap[tile.edges.n.data]; len(tileList) > 1 {
			tile.connections++
			if tileList[0] != id {
				tile.edges.n.tile = tileList[0]
			} else {
				tile.edges.n.tile = tileList[1]
			}
		}
		if tileList, _ := edgeMap[tile.edges.s.data]; len(tileList) > 1 {
			tile.connections++
			if tileList[0] != id {
				tile.edges.s.tile = tileList[0]
			} else {
				tile.edges.s.tile = tileList[1]
			}
		}
		if tileList, _ := edgeMap[tile.edges.w.data]; len(tileList) > 1 {
			tile.connections++
			if tileList[0] != id {
				tile.edges.w.tile = tileList[0]
			} else {
				tile.edges.w.tile = tileList[1]
			}
		}
		if tile.connections < 2 {
			panic("not enough connections")
		}

	}

	return
}

func reverseRunes(r []rune) []rune {
	reverse := make([]rune, len(r))
	copy(reverse, r)
	for i := 0; i < len(reverse)/2; i++ {
		reverse[i], reverse[len(reverse)-1-i] = reverse[len(reverse)-1-i], reverse[i]
	}
	return reverse
}

func reverseString(s string) string {
	reverse := []rune(s)
	for i := 0; i < len(reverse)/2; i++ {
		reverse[i], reverse[len(reverse)-1-i] = reverse[len(reverse)-1-i], reverse[i]
	}
	return string(reverse)
}

func contains(is []int, i int) bool {
	for _, c := range is {
		if i == c {
			return true
		}
	}

	return false
}
