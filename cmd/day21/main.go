package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

func main() {
	part2()
}

func part2() {
	iMap := loadIngredients()

	ingredientStrings := []string{}

	for k, v := range iMap {
		ingredientStrings = append(ingredientStrings, v+":"+k)
	}

	sort.Strings(ingredientStrings)

	r := ""

	for i, j := range ingredientStrings {
		r += strings.Split(j, ":")[1]
		if i != len(ingredientStrings)-1 {
			r += ","
		}
	}

	fmt.Println(r)
}

func part1() {
	ingredientsMap := loadIngredients()

	d, err := ioutil.ReadFile("./cmd/day21/data")
	if err != nil {
		panic(err)
	}

	count := 0
	for _, line := range strings.Split(string(d), "\n") {
		parts := strings.Split(line, " (contains ")
		ingredients := strings.Split(parts[0], " ")
		for _, i := range ingredients {
			if _, ok := ingredientsMap[i]; !ok {
				count++
			}
		}
	}

	fmt.Println(count)
}

func loadIngredients() map[string]string {
	d, err := ioutil.ReadFile("./cmd/day21/data")
	if err != nil {
		panic(err)
	}

	allergensMap := map[string][]string{}

	for _, line := range strings.Split(string(d), "\n") {
		parts := strings.Split(line, " (contains ")
		ingredients := strings.Split(parts[0], " ")
		allergens := strings.Split(strings.TrimRight(parts[1], ")"), ", ")
		for _, a := range allergens {
			if curr, ok := allergensMap[a]; ok {
				allergensMap[a] = intersection(curr, ingredients)
			} else {
				c := make([]string, len(ingredients))
				copy(c, ingredients)
				allergensMap[a] = c
			}
		}
	}

	for {
		repeat := false

		for k, v := range allergensMap {
			if len(v) > 1 {
				repeat = true
				continue
			}
			for i, j := range allergensMap {
				if i == k {
					continue
				}
				allergensMap[i] = remove(j, v[0])
			}
		}

		if !repeat {
			break
		}
	}

	ingredientsMap := map[string]string{}

	for k, v := range allergensMap {
		ingredientsMap[v[0]] = k
	}
	return ingredientsMap
}

func remove(ss []string, s string) []string {
	r := []string{}
	for _, i := range ss {
		if i != s {
			r = append(r, i)
		}
	}
	return r
}

func intersection(a, b []string) []string {
	result := []string{}
outer:
	for _, i := range a {
		for _, j := range b {
			if i == j {
				result = append(result, i)
				continue outer
			}
		}
	}

	return result
}
