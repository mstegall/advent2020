package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func score(d []int) int {
	dLen := len(d)
	score := 0
	for i, v := range d {
		score += (dLen - i) * v
	}
	return score
}

func part2() {
	d1, d2 := loadDecks()

	_, winner := playRecursive(d1, d2)

	fmt.Println(score(winner))
}

func part1() {
	d1, d2 := loadDecks()

	for len(d1) != 0 && len(d2) != 0 {
		p1, p2 := d1[0], d2[0]
		d1, d2 = d1[1:], d2[1:]

		if p1 > p2 {
			d1 = append(d1, p1, p2)
		} else {
			d2 = append(d2, p2, p1)
		}
	}

	var winner []int
	if len(d1) > 0 {
		winner = d1
	} else {
		winner = d2
	}

	fmt.Println(score(winner))
}

func playRecursive(d1, d2 []int) (bool, []int) {
	seenMap := map[string]bool{}

	for len(d1) != 0 && len(d2) != 0 {
		d1String := ""
		d2String := ""

		for _, c := range d1 {
			d1String += string(c)
		}

		for _, c := range d2 {
			d2String += string(c)
		}

		_, ok1 := seenMap[d1String]
		_, ok2 := seenMap[d2String]
		if ok1 || ok2 {
			return true, d1
		}

		seenMap[d1String] = true
		seenMap[d2String] = true

		p1, p2 := d1[0], d2[0]
		d1, d2 = d1[1:], d2[1:]

		var p1Wins bool
		if len(d1) >= p1 && len(d2) >= p2 {
			newD1 := make([]int, p1)
			newD2 := make([]int, p2)
			copy(newD1, d1[:p1])
			copy(newD2, d2[:p2])

			p1Wins, _ = playRecursive(newD1, newD2)
		} else {
			p1Wins = p1 > p2
		}

		if p1Wins {
			d1 = append(d1, p1, p2)
		} else {
			d2 = append(d2, p2, p1)
		}
	}

	if len(d1) > 0 {
		return true, d1
	}

	return false, d2
}

func loadDecks() ([]int, []int) {
	d, err := ioutil.ReadFile("./cmd/day22/data")
	if err != nil {
		panic(err)
	}

	players := strings.Split(string(d), "\n\n")
	deck1 := []int{}
	deck2 := []int{}

	for _, card := range strings.Split(players[0], "\n")[1:] {
		i, err := strconv.Atoi(card)
		if err != nil {
			panic(err)
		}
		deck1 = append(deck1, i)
	}

	for _, card := range strings.Split(players[1], "\n")[1:] {
		i, err := strconv.Atoi(card)
		if err != nil {
			panic(err)
		}
		deck2 = append(deck2, i)
	}

	return deck1, deck2
}
