package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"strconv"
)

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	// part3()
	// part1()
	part2()
}

type node struct {
	v    int
	next *node
}

func printList(n *node) {
	v, p := n.v, n
	out := ""
	out += strconv.Itoa(p.v)
	p = p.next
	for p.v != v {
		out += strconv.Itoa(p.v)
		p = p.next
	}
	fmt.Println(out)
}

func part1() {
	board := []int{3, 6, 4, 2, 9, 7, 5, 8, 1}
	// board := []int{3, 8, 9, 1, 2, 5, 4, 6, 7}
	start := &node{v: board[0]}
	head := start

	for i, v := range board[1:] {
		head.next = &node{v: v}
		head = head.next

		if i == len(board)-2 {
			head.next = start
			head = head.next
		}
	}

	for i := 0; i < 100; i++ {
		chunk := head.next
		head.next = chunk.next.next.next
		destination := head.v - 1
		if destination == 0 {
			destination = 9
		}
		for chunk.v == destination || chunk.next.v == destination || chunk.next.next.v == destination {
			destination--
			if destination == 0 {
				destination = 9
			}
		}

		dNode := head
		for dNode.v != destination {
			dNode = dNode.next
		}
		chunk.next.next.next = dNode.next
		dNode.next = chunk

		head = head.next

		// printList(head)
	}

	for head.v != 1 {
		head = head.next
	}

	head = head.next
	out := ""

	for head.v != 1 {
		out += strconv.Itoa(head.v)
		head = head.next
	}

	fmt.Println(out)
}

// Now a higher performance version of part3, turns out array lookups are faster than struct proptery access
func part2() {
	board := []int{3, 6, 4, 2, 9, 7, 5, 8, 1}
	// board := []int{3, 8, 9, 1, 2, 5, 4, 6, 7}

	for i := len(board) + 1; i <= 1000000; i++ {
		board = append(board, i)
	}

	// boardMap := map[int]int{}
	boardMap := make([]int, 1000001)

	head := 3

	for i, v := range board {
		if i == len(board)-1 {
			boardMap[v] = board[0]
			continue
		}

		boardMap[v] = board[i+1]

	}

	highest := len(board)

	for i := 0; i < 10000000; i++ {
		chunk1 := boardMap[head]
		chunk2 := boardMap[chunk1]
		chunk3 := boardMap[chunk2]
		boardMap[head] = boardMap[chunk3]
		destination := head - 1
		if destination == 0 {
			destination = highest
		}
		for chunk1 == destination || chunk2 == destination || chunk3 == destination {
			destination--
			if destination == 0 {
				destination = highest
			}
		}

		boardMap[chunk3] = boardMap[destination]
		boardMap[destination] = chunk1

		head = boardMap[head]
	}

	one := boardMap[1]
	two := boardMap[one]

	fmt.Println(one * two)
}

// Higher performance version of part2 using map lookups only for resolving the destination to a node
func part3() {
	board := []int{3, 6, 4, 2, 9, 7, 5, 8, 1}
	// board := []int{3, 8, 9, 1, 2, 5, 4, 6, 7}

	for i := len(board) + 1; i <= 1000000; i++ {
		board = append(board, i)
	}

	boardMap := make([]*node, 1000001)

	var start *node
	head := &node{}

	for i, v := range board {
		head.next = &node{v: v}
		head = head.next
		if start == nil {
			start = head
		}
		boardMap[v] = head

		if i == len(board)-1 {
			head.next = start
			head = head.next
		}

	}

	highest := len(board)

	for i := 0; i < 10000000; i++ {
		chunk := head.next
		chunkTail := chunk.next.next
		head.next = chunkTail.next

		destination := head.v - 1
		if destination == 0 {
			destination = highest
		}
		for chunk.v == destination || chunk.next.v == destination || chunk.next.next.v == destination {
			destination--
			if destination == 0 {
				destination = highest
			}
		}

		dNode := boardMap[destination]

		chunkTail.next = dNode.next
		dNode.next = chunk

		head = head.next

	}

	one := boardMap[1].next
	two := one.next

	fmt.Println(one.v * two.v)
}
