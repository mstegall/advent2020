package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	part2()
}

var dirMap = map[string][2]int{
	"e":  {2, 0},
	"w":  {-2, 0},
	"nw": {-1, 1},
	"ne": {1, 1},
	"sw": {-1, -1},
	"se": {1, -1},
}

func part1() {
	grid := loadGrid()

	fmt.Println(grid.countLive())
}

func part2() {
	g := loadGrid()

	for i := 0; i < 100; i++ {
		points := g.eligiblePoints()
		newGrid := &grid{
			data: map[string]bool{},
		}

		for _, p := range points {
			if g.nextPointState(p[0], p[1]) {
				newGrid.data[strconv.Itoa(p[0])+":"+strconv.Itoa(p[1])] = true
			}
		}

		g = newGrid
	}

	fmt.Println(g.countLive())
}

type grid struct {
	data map[string]bool
}

func (g *grid) isLive(x, y int) bool {
	return g.data[strconv.Itoa(x)+":"+strconv.Itoa(y)]
}

func (g *grid) countLive() int {
	count := 0

	for _, v := range g.data {
		if v {
			count++
		}
	}

	return count
}

func (g *grid) eligiblePoints() [][2]int {
	eMap := map[string]bool{}

	for k, v := range g.data {
		if v == false {
			continue
		}

		ps := strings.Split(k, ":")
		x, err := strconv.Atoi(ps[0])
		if err != nil {
			panic(err)
		}
		y, err := strconv.Atoi(ps[1])
		if err != nil {
			panic(err)
		}

		for _, dP := range dirMap {
			eMap[strconv.Itoa(x+dP[0])+":"+strconv.Itoa(y+dP[1])] = true
		}
	}

	results := [][2]int{}

	for k := range eMap {
		ps := strings.Split(k, ":")
		x, err := strconv.Atoi(ps[0])
		if err != nil {
			panic(err)
		}
		y, err := strconv.Atoi(ps[1])
		if err != nil {
			panic(err)
		}
		results = append(results, [2]int{x, y})
	}

	return results
}

func (g *grid) nextPointState(x, y int) bool {
	count := 0
	for _, v := range dirMap {
		if g.isLive(x+v[0], y+v[1]) {
			count++
		}
	}

	if !g.isLive(x, y) {
		return count == 2
	}
	return count == 1 || count == 2
}

func loadGrid() *grid {
	d, err := ioutil.ReadFile("./cmd/day24/data")
	if err != nil {
		panic(err)
	}

	data := map[string]bool{}

	for _, line := range strings.Split(string(d), "\n") {
		line := []rune(line)
		x, y := 0, 0
		for len(line) > 0 {
			var dir string
			if line[0] == 'n' || line[0] == 's' {
				dir = string(line[:2])
				line = line[2:]
			} else {
				dir = string(line[0])
				line = line[1:]
			}

			dP := dirMap[dir]
			x += dP[0]
			y += dP[1]
		}

		point := strconv.Itoa(x) + ":" + strconv.Itoa(y)
		if v, ok := data[point]; ok {
			data[point] = !v
		} else {
			data[point] = true
		}
	}
	return &grid{data}
}
