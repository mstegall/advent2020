package main

import "fmt"

func main() {
	part1()
}

const door = 12090988
const card = 240583

func part1() {
	fmt.Println(transform(door, findIter(7, card)))
}

func transform(sub, i int) int {
	v := 1
	for j := 0; j < i; j++ {
		v *= sub
		v %= 20201227
	}

	return v
}

func findIter(sub, key int) int {
	count := 0
	i := 1
	for i != key {
		i *= sub
		i %= 20201227
		count++
	}
	return count
}
