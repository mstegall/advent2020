package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	part2()
}

func getMap() (data [][]string) {
	file, err := os.Open("./cmd/day3/data")
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		data = append(data, strings.Split(scanner.Text(), ""))
	}

	return
}

func getTrees(dx, dy int) int {
	mapData := getMap()
	x, y := 0, 0
	mapWidth := len(mapData[0])
	treeCount := 0

	for y < len(mapData) {
		if mapData[y][x] == "#" {
			treeCount++
		}

		y += dy
		x += dx
		x %= mapWidth
	}

	return treeCount
}

func part1() {
	fmt.Println(getTrees(3, 1))
}

func part2() {
	fmt.Println(getTrees(1, 1) * getTrees(3, 1) * getTrees(5, 1) * getTrees(7, 1) * getTrees(1, 2))
}
