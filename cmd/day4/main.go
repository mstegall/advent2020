package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	part2()
}

type passport struct {
	byr string
	iyr string
	eyr string
	hgt string
	hcl string
	ecl string
	pid string
	cid string
}

func parsePassport(data string) (p passport, err error) {
	for _, line := range strings.Split(data, "\n") {
		for _, word := range strings.Split(line, " ") {
			parts := strings.Split(word, ":")
			switch parts[0] {
			case "byr":
				p.byr = parts[1]
			case "iyr":
				p.iyr = parts[1]
			case "eyr":
				p.eyr = parts[1]
			case "hgt":
				p.hgt = parts[1]
			case "hcl":
				p.hcl = parts[1]
			case "ecl":
				p.ecl = parts[1]
			case "pid":
				p.pid = parts[1]
			case "cid":
				p.cid = parts[1]
			}
		}
	}

	if p.byr == "" || p.ecl == "" || p.eyr == "" || p.hcl == "" || p.hgt == "" || p.iyr == "" || p.pid == "" {
		return passport{}, fmt.Errorf("data missing")
	}

	return
}

func (p passport) validate() bool {
	byr, err := strconv.Atoi(p.byr)
	if err != nil || byr < 1920 || byr > 2002 {
		return false
	}

	iyr, err := strconv.Atoi(p.iyr)
	if err != nil || iyr < 2010 || iyr > 2020 {
		return false
	}

	eyr, err := strconv.Atoi(p.eyr)
	if err != nil || eyr < 2020 || eyr > 2030 {
		return false
	}

	if strings.HasSuffix(p.hgt, "in") {
		hgt, err := strconv.Atoi(strings.TrimRight(p.hgt, "in"))
		if err != nil || hgt < 59 || hgt > 76 {
			return false
		}
	} else if strings.HasSuffix(p.hgt, "cm") {
		hgt, err := strconv.Atoi(strings.TrimRight(p.hgt, "cm"))
		if err != nil || hgt < 150 || hgt > 193 {
			return false
		}
	} else {
		return false
	}

	if !regexp.MustCompile(`^#[0-9a-f]{6}$`).Match([]byte(p.hcl)) {
		return false
	}

	if !regexp.MustCompile(`^\d{9}$`).Match([]byte(p.pid)) {
		return false
	}

	validEyes := []string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
	for _, eye := range validEyes {
		if eye == p.ecl {
			return true
		}
	}

	return false
}

func part1() {
	data, err := ioutil.ReadFile("./cmd/day4/data")
	if err != nil {
		panic(err)
	}
	validCount := 0
	for _, chunk := range strings.Split(string(data), "\n\n") {
		_, err := parsePassport(chunk)
		if err == nil {
			validCount++
		}
	}

	fmt.Println(validCount)
}

func part2() {
	data, err := ioutil.ReadFile("./cmd/day4/data")
	if err != nil {
		panic(err)
	}
	validCount := 0
	for _, chunk := range strings.Split(string(data), "\n\n") {
		p, err := parsePassport(chunk)
		if err != nil {
			continue
		}
		if p.validate() {
			validCount++
		}
	}

	fmt.Println(validCount)
}
