package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

func main() {
	part2()
}

func seatID(desc string) (i int) {
	for _, char := range strings.Split(desc, "") {
		i <<= 1

		if char == "B" || char == "R" {
			i++
		}
	}

	return
}

func part1() {
	file, err := os.Open("./cmd/day5/data")
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	max := 0

	for scanner.Scan() {
		newID := seatID(scanner.Text())

		if newID > max {
			max = newID
		}
	}

	fmt.Println(max)
}

func part2() {
	file, err := os.Open("./cmd/day5/data")
	if err != nil {
		panic(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	seats := []int{}

	for scanner.Scan() {
		seats = append(seats, seatID(scanner.Text()))
	}

	sort.Ints(seats)

	for i, seat := range seats[1:] {
		if seat-1 != seats[i] {
			fmt.Println(seat - 1)
		}
	}
}
