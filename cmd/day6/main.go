package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	part2()
}

func groupCount(group string) int {
	data := make(map[string]bool)

	for _, line := range strings.Split(group, "\n") {
		for _, char := range strings.Split(line, "") {
			data[char] = true
		}
	}

	fmt.Println(len(data))
	return len(data)
}

func inSlice(a string, b []string) bool {
	for _, b := range b {
		if a == b {
			return true
		}
	}

	return false
}

func groupCount2(group string) int {
	lines := strings.Split(group, "\n")

	intersection := strings.Split(lines[0], "")

	for _, line := range lines[1:] {
		chars := strings.Split(line, "")

		next := []string{}

		for _, j := range intersection {
			if inSlice(j, chars) {
				next = append(next, j)
			}
		}

		intersection = next
	}

	return len(intersection)
}

func part1() {
	data, err := ioutil.ReadFile("./cmd/day6/data")
	if err != nil {
		panic(err)
	}
	sum := 0

	for _, chunk := range strings.Split(string(data), "\n\n") {
		sum += groupCount(chunk)
	}

	fmt.Println(sum)
}

func part2() {
	data, err := ioutil.ReadFile("./cmd/day6/data")
	if err != nil {
		panic(err)
	}
	sum := 0

	for _, chunk := range strings.Split(string(data), "\n\n") {
		sum += groupCount2(chunk)
	}

	fmt.Println(sum)
}
