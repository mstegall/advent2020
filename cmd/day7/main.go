package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	part2()
}

func part1() {
	_, containedBy := getMaps()

	fmt.Println(getContainers("shiny gold", containedBy))
}

func part2() {
	contains, _ := getMaps()

	fmt.Println(getBags("shiny gold", contains))
}

type bagNode struct {
	Bag   string
	Count int
}

type bagMap = map[string][]bagNode

func getMaps() (contains bagMap, containedBy bagMap) {
	data, err := ioutil.ReadFile("./cmd/day7/data")
	if err != nil {
		panic(err)
	}

	contains = make(map[string][]bagNode)
	containedBy = make(map[string][]bagNode)

	for _, line := range strings.Split(string(data), "\n") {
		parts := strings.Split(line, " contain ")

		outer := strings.TrimSuffix(parts[0], " bags")
		contains[outer] = []bagNode{}
		inners := strings.Split(strings.Trim(parts[1], "."), ", ")
		for _, inner := range inners {
			if inner == "no other bags" {
				continue
			}

			trimmed := strings.TrimSuffix(inner, " bags")
			trimmed = strings.TrimSuffix(trimmed, " bag")

			innerParts := strings.Split(trimmed, " ")

			number, err := strconv.Atoi(innerParts[0])
			if err != nil {
				panic(err)
			}
			name := strings.Join(innerParts[1:], " ")

			contains[outer] = append(contains[outer], bagNode{
				Bag:   name,
				Count: number,
			})

			if containedBy[name] == nil {
				containedBy[name] = []bagNode{}
			}

			containedBy[name] = append(containedBy[name], bagNode{
				Bag:   outer,
				Count: number,
			})
		}
	}

	return
}

func getContainers(bag string, containedBy bagMap) int {
	resultSet := &map[string]bool{}

	getContainersInner(bag, containedBy, resultSet)

	return len(*resultSet)
}

func getContainersInner(bag string, containedBy bagMap, result *map[string]bool) {
	containers := containedBy[bag]

	fmt.Println(bag, containers)

	res := *result
	for _, container := range containers {
		res[container.Bag] = true

		getContainersInner(container.Bag, containedBy, result)
	}

	return
}

func getBags(bag string, contains bagMap) int {
	count := 0

	for _, inner := range contains[bag] {
		count += inner.Count * (1 + getBags(inner.Bag, contains))
	}

	return count
}
