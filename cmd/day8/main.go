package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	part2()
}

type instruction struct {
	op  string
	arg int
}

type machine struct {
	instructions []instruction
	register     int
	pc           int
	visited      map[int]bool
}

func (m *machine) loadInstructions() {
	data, err := ioutil.ReadFile("./cmd/day8/data")
	// data, err := ioutil.ReadFile("./data")
	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(data), "\n") {
		parts := strings.Split(line, " ")

		op := parts[0]
		arg, err := strconv.Atoi(parts[1])
		if err != nil {

			panic(err)
		}

		m.instructions = append(m.instructions, instruction{op, arg})
	}
}

func (m *machine) execUntilLoop() bool {
	for !m.visited[m.pc] && m.pc < len(m.instructions) {
		m.visited[m.pc] = true

		inst := m.instructions[m.pc]

		switch inst.op {
		case "jmp":
			m.pc += inst.arg
			continue
		case "acc":
			m.register += inst.arg
		}

		m.pc++
	}

	return m.pc < len(m.instructions)
}

func (m *machine) reset() {
	m.pc = 0
	m.register = 0
	m.visited = make(map[int]bool)
}

func (m *machine) findBug() {
	for instID := 0; instID < len(m.instructions); instID++ {
		originalInst := m.instructions[instID]
		inst := originalInst
		switch inst.op {
		case "acc":
			continue
		case "nop":
			inst.op = "jmp"
		case "jmp":
			inst.op = "nop"
		}

		m.instructions[instID] = inst
		m.reset()
		if !m.execUntilLoop() {
			return
		}
		m.instructions[instID] = originalInst
	}

	panic("AAAAA")
}

func newMachine() *machine {
	return &machine{
		visited: make(map[int]bool),
	}
}

func part1() {
	m := newMachine()
	m.loadInstructions()

	m.execUntilLoop()

	fmt.Println(m.register)
}

func part2() {
	m := newMachine()
	m.loadInstructions()

	m.findBug()

	fmt.Println(m.register)
}
