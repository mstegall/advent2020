package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	fmt.Println(part2())
}

func loadData() (data []int) {
	d, err := ioutil.ReadFile("./cmd/day9/data")
	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(string(d), "\n") {
		num, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}
		data = append(data, num)
	}

	return
}

func hasSum(data []int, sum int) bool {
	for ii, i := range data {
		for _, j := range data[ii:] {
			if i+j == sum {
				return true
			}
		}
	}

	return false
}

func sum(data []int) (sum int) {
	for _, i := range data {
		sum += i
	}

	return
}

func part1() int {
	data := loadData()

	for i := 25; i < len(data); i++ {
		if !hasSum(data[i-25:i], data[i]) {
			return data[i]
		}
	}

	panic("no error")
}

func part2() int {
	data := loadData()
	target := part1()
	lower, upper := 0, 0

	for {
		s := sum(data[lower:upper])

		if s == target {
			match := data[lower:upper]
			min, max := match[0], match[0]
			for _, i := range match {
				if i < min {
					min = i
				}

				if i > max {
					max = i
				}
			}

			return min + max
		}

		if s < target {
			upper++
			if upper >= len(data) {
				panic("ran out of numbers")
			}
		} else {
			lower++
		}
	}
}
